Rails.application.routes.draw do
  root 'users#index'
  get '/welcome/', to: 'users#welcome'
  get '/:id/invite_event', to: 'users#invite_event', as: 'invite'
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
