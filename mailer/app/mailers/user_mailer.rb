class UserMailer < ApplicationMailer
  include ApplicationHelper

  def invite_event(user1, user2)
    @name1 = get_name(user1)
    @user = user1
    @name2 = get_name(user2)
    mail(from: @user.email, to: user2.email, subject: "Invite event")
  end

  def welcome(user)
    @name = get_name(user)
    mail(to: user.email, subject: "Welcome")
  end
end
